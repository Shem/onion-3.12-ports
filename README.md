# Onion 3.12 ports

This folder contains all the ports originally included in Onion OS 3.12. It's hosted here in case there was an issue adding the ports during install.

Drag the EMU folder onto your Miyoo Mini SD card to add the 3.12 ports to the ports system.
